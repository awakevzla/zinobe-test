<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $table = "countries";
    protected $fillable = ["code","name"];
    public $timestamps = ["created_at","updated_at"];
}