<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    protected $table = "users";
    protected $fillable = ["name","email","country_id","password"];
    public $timestamps = ["created_at","updated_at"];

    public function country()
    {
        return $this->belongsTo('Models\Country','country_id');
    }
}