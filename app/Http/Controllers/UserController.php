<?php

namespace Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Redirect;
use Models\Country;
use Models\User;

class UserController {

    public $twig;

    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/../../../templates');

        $this->twig = new \Twig_Environment($loader);
    }

    public function index(Request $request)
    {
        $authUser = (isset($_SESSION["user_id"]))?$_SESSION["user_id"]:0;

        if($authUser) {
            $this->redirect('?error=You cant register again.');
        }

        $message = $request->exists('message')?$request->get('message'):'';
        $error = $request->exists('error')?$request->get('error'):'';

        $countries = Country::all();
        return $this->twig->render('register.html', ['base_url'=>APP_URL,'authUser'=>$authUser,'countries'=>$countries,'message'=>$message,'error'=>$error]);
    }

    public function store(Request $request) {
        $email = ($request->get('email'))?$request->get('email'):'';
        $name = ($request->get('name'))?$request->get('name'):'';
        $password = ($request->get('password'))?$request->get('password'):'';
        $country_id = ($request->get('country_id'))?$request->get('country_id'):'';
        $password_confirm = ($request->get('password_confirm'))?$request->get('password_confirm'):'';
        $country = Country::find($country_id);

        if (!$country) {
            $this->redirect('/users?message=Country not found!');
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->redirect('/users?message=Email not valid!');
        }
        if (!$name) {
            $this->redirect('/users?error=Name is required!');
        }
        if (!$password) {
            $this->redirect('/users?message=Password is required');
        }
        if($password != $password_confirm) {
            $this->redirect('/users?message=Password doesnt match!');
        }
        try{
            User::create(["name"=>$name,"email"=>$email, "country_id"=>$country->id, "password"=>password_hash($password, PASSWORD_DEFAULT)]);
        }catch (\Exception $e){
            $this->redirect('/users?message=Password doesnt match!');
        }

        $this->redirect('/auth?message=User Created, log in now!');
    }

    public function search(Request $request)
    {
        $users = User::with('country')->get();
        $authUser = (isset($_SESSION["user_id"]))?$_SESSION["user_id"]:0;
        if (!$authUser) {
            $this->redirect('/');
        }

        if ($request->get('search')) {
            $users = User::query()
                ->where('name','like', '%'.$request->get('search').'%')
                ->orWhere('email', 'like', '%'.$request->get('search').'%')
                ->get();
        }

        return $this->twig->render('search.html', ['base_url'=>APP_URL, 'users'=>$users,'authUser'=>$authUser]);
    }

    public function redirect($url)
    {
        header('Location: '.APP_URL.$url);
        exit();
    }
}