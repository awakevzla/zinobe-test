<?php
namespace Controllers;


use Illuminate\Http\Request;
use Models\User;

class AuthController {

    public $twig;

    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/../../../templates');

        $this->twig = new \Twig_Environment($loader);
    }

    public function getLogin(Request $request)
    {
        $authUser = (isset($_SESSION["user_id"]))?$_SESSION["user_id"]:false;

        if($authUser) {
            $this->redirect('?error=You are already logged in.');
        }

        $error = $request->exists('error')?$request->get('error'):"";

        $message = $request->exists('message')?$request->get('message'):"";

        return $this->twig->render('login.html', ['base_url'=>APP_URL,'authUser'=>$authUser,'message'=>$message,'error'=>$error]);
    }

    public function postLogin(Request $request)
    {
        $user = User::query()
            ->where('email','=',$request->get('email'))
            ->first();

        if (!$user){
            $this->redirect('/auth?error=Email not found!');
        }

        if (!password_verify($request->get('password'), $user->password)) {
            $this->redirect('/auth?error=Password doesnt match!');
        }

        $_SESSION["user_id"] = $user->id;
        $_SESSION["user_name"] = $user->name;
        $_SESSION["user_email"] = $user->email;

        $this->redirect('/');
    }

    public function logout()
    {
        session_unset();
        $this->redirect('/');
    }

    public function redirect($url)
    {
        header('Location: '.APP_URL.$url);
        exit();
    }
}