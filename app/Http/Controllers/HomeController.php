<?php

namespace Controllers;

use Illuminate\Http\Request;

class HomeController {

    public $twig;

    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/../../../templates');

        $this->twig = new \Twig_Environment($loader);
    }

    public function index(Request $request)
    {
        $authUser = (isset($_SESSION["user_id"]))?$_SESSION["user_id"]:false;
        $userName = (isset($_SESSION["user_name"]))?$_SESSION["user_name"]:"Anonymous";

        $error = $request->exists('error')?$request->get('error'):"";

        return $this->twig->render('home.html', ['base_url'=>APP_URL,'authUser'=>$authUser,'error'=>$error,'userName'=>$userName]);
    }
}