<?php

require 'start-app.php';
$container = new \Illuminate\Container\Container();

$request = \Illuminate\Http\Request::capture();
$container->instance('Illuminate\Http\Request', $request);

$events = new \Illuminate\Events\Dispatcher($container);

$router = new \Illuminate\Routing\Router($events, $container);

$sessionManager = new \Illuminate\Session\SessionManager($container);

require_once 'router.php';

$redirect = new \Illuminate\Routing\Redirector(new \Illuminate\Routing\UrlGenerator($router->getRoutes(), $request));

$response = $router->dispatch($request);

$response->send();