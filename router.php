<?php
session_start();
use Illuminate\Routing\Router;

$router->group(['namespace' => 'Controllers', 'prefix' => 'users'], function (Router $router) {
    $router->get('/', ['name' => 'users.index', 'uses' => 'UserController@index']);
    $router->get('/search', ['name' => 'users.search', 'uses' => 'UserController@search']);
    $router->post('/', ['name' => 'users.store', 'uses' => 'UserController@store']);
});

$router->group(['namespace' => 'Controllers'], function (Router $router) {
    $router->get('/', ['name'=>'home', 'uses'=>'HomeController@index']);
});

$router->group(['namespace' => 'Controllers','prefix'=>'auth'], function (Router $router) {
    $router->get('/', ['name'=>'getLogin', 'uses'=>'AuthController@getLogin']);
    $router->post('/', ['name'=>'postLogin', 'uses'=>'AuthController@postLogin']);
    $router->get('/logout', ['name'=>'logout', 'uses'=>'AuthController@logout']);
});

$router->any('{any}', function () {
    return '404 Not found!';
})->where('any', '(.*)');